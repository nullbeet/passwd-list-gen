from random import randint


with open('nouns', 'r') as file:
    nouns = file.read()

with open('adjs', 'r') as file:
    adjs = file.read()

adjs_list = adjs.splitlines()
noun_list = nouns.splitlines()

def rand_pass():
    rand_num = str(randint(100, 999))
    rand_adj = adjs_list[randint(0, len(adjs_list) - 1)]
    rand_noun = noun_list[randint(0, len(noun_list) - 1)]

    return rand_adj + rand_noun + rand_num + '\n'


for i in range(0, len(adjs_list) * len(noun_list) * 899):
    with open('pass-list', 'a+') as file:
        passwd = rand_pass()
        existing = file.read()    
        if passwd in existing.splitlines():
            pass
        else:
            file.write(passwd)
